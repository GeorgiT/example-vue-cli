import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Registration from '@/components/LoginPageComponents/RegistrationSection'
import Firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/login',
      redirect: '/'
    },
    {
      path: '/',
      name: 'LoginPage',
      component: Login
    },
    {
      path: '/home',
      name: 'HomePage',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/registration',
      name: 'RegistrationPage',
      component: Registration
    }
  ]
})

router.beforeEach((to, from, next) => {
  const currentUser = Firebase.auth().currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !currentUser) {
    next('/')
  } else if (!requiresAuth && currentUser) {
    next('home')
  } else {
    next()
  }
})

export default router
