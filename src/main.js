// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '../node_modules/bulma/css/bulma.css'
import '../src/assets/css/style.css'
import VueFire from 'vuefire'
import Firebase from 'firebase'
import Chart from 'chart.js'
import VueCharts from 'hchs-vue-charts'

Vue.use(VueFire)
Vue.use(VueCharts, Chart)

Vue.config.productionTip = false
// Initialize Firebase
var app
var config = {
  apiKey: 'AIzaSyDCNegtZp8z6V43zGHsf4OnbufqWNh9lTs',
  authDomain: 'vue-dashboard-b683c.firebaseapp.com',
  databaseURL: 'https://vue-dashboard-b683c.firebaseio.com',
  projectId: 'vue-dashboard-b683c',
  storageBucket: '',
  messagingSenderId: '868052476896'
}
Firebase.initializeApp(config)
Firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      components: {App},
      template: '<App/>'
    })
  }
})
